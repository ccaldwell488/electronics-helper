﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace projElectronicsHelper
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void lbo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateData(1, lbo1.SelectedItem.ToString());
        }
        
        private void lbo2_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateData(2, lbo2.SelectedItem.ToString());
        }

        private void lbo3_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateData(3, lbo3.SelectedItem.ToString());
        }

        private void updateData(int targetPanel, string colChoice)
        {
            lblResult.Text = "";
            Color c = Color.FromName(colChoice);

            switch (targetPanel)
            {
                case 1:
                    panel1.BackColor = c;
                    break;
                case 2:
                    panel2.BackColor = c;
                    break;
                case 3:
                    panel3.BackColor = c;
                    break;
                default:
                    break;
            }

        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            // if Three values have been selected
            if ((lbo1.SelectedIndex != -1) &&
                (lbo2.SelectedIndex != -1) &&
                (lbo3.SelectedIndex != -1))
            {

                int band1 = lbo1.SelectedIndex * 10; // tens
                int band2 = lbo2.SelectedIndex; //units
                double band3 = Math.Pow(10, lbo3.SelectedIndex);// Multiplier
                
                double value = (band1 + band2) * band3;

                string humanReadable = "";

                // Pretty print large value
                if ((value >= 1000) && (value <= 999999)) {
                    humanReadable = "K ";
                    value = value / 1000;
                }
                else if (value > 999999)
                {
                    humanReadable = "M ";
                    value = value / 1000000;
                }

                lblResult.Text = string.Format("{0} {1}{2}", value, humanReadable, (char)'\u2126');
                lblResult.TextAlign = ContentAlignment.MiddleCenter;
            }
            else
            {
                MessageBox.Show("Select three colour values first!");
            }
        }
    }
}
