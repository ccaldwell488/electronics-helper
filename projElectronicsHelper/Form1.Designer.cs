﻿namespace projElectronicsHelper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbo1 = new System.Windows.Forms.ListBox();
            this.lbo2 = new System.Windows.Forms.ListBox();
            this.lbo3 = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.picResistor = new System.Windows.Forms.PictureBox();
            this.picRainbow = new System.Windows.Forms.PictureBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.btnCalc = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picResistor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRainbow)).BeginInit();
            this.SuspendLayout();
            // 
            // lbo1
            // 
            this.lbo1.BackColor = System.Drawing.Color.White;
            this.lbo1.FormattingEnabled = true;
            this.lbo1.Items.AddRange(new object[] {
            "Black",
            "Brown",
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue",
            "Violet",
            "Gray",
            "White"});
            this.lbo1.Location = new System.Drawing.Point(104, 29);
            this.lbo1.Name = "lbo1";
            this.lbo1.Size = new System.Drawing.Size(52, 134);
            this.lbo1.TabIndex = 0;
            this.lbo1.SelectedIndexChanged += new System.EventHandler(this.lbo1_SelectedIndexChanged);
            // 
            // lbo2
            // 
            this.lbo2.BackColor = System.Drawing.Color.White;
            this.lbo2.FormattingEnabled = true;
            this.lbo2.Items.AddRange(new object[] {
            "Black",
            "Brown",
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue",
            "Violet",
            "Gray",
            "White"});
            this.lbo2.Location = new System.Drawing.Point(162, 29);
            this.lbo2.Name = "lbo2";
            this.lbo2.Size = new System.Drawing.Size(52, 134);
            this.lbo2.TabIndex = 2;
            this.lbo2.SelectedIndexChanged += new System.EventHandler(this.lbo2_SelectedIndexChanged);
            // 
            // lbo3
            // 
            this.lbo3.BackColor = System.Drawing.Color.White;
            this.lbo3.FormattingEnabled = true;
            this.lbo3.Items.AddRange(new object[] {
            "Black",
            "Brown",
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue",
            "Violet",
            "Gray",
            "White"});
            this.lbo3.Location = new System.Drawing.Point(220, 29);
            this.lbo3.Name = "lbo3";
            this.lbo3.Size = new System.Drawing.Size(52, 134);
            this.lbo3.TabIndex = 3;
            this.lbo3.SelectedIndexChanged += new System.EventHandler(this.lbo3_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Location = new System.Drawing.Point(117, 230);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(15, 66);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel2.Location = new System.Drawing.Point(159, 238);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(15, 49);
            this.panel2.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.Location = new System.Drawing.Point(185, 238);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(15, 49);
            this.panel3.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "BAND 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "BAND 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(217, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "BAND 3";
            // 
            // picResistor
            // 
            this.picResistor.BackColor = System.Drawing.Color.Transparent;
            this.picResistor.Image = global::projElectronicsHelper.Properties.Resources.resistorBands;
            this.picResistor.Location = new System.Drawing.Point(43, 226);
            this.picResistor.Name = "picResistor";
            this.picResistor.Size = new System.Drawing.Size(280, 74);
            this.picResistor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picResistor.TabIndex = 9;
            this.picResistor.TabStop = false;
            // 
            // picRainbow
            // 
            this.picRainbow.Image = global::projElectronicsHelper.Properties.Resources.rainbow;
            this.picRainbow.Location = new System.Drawing.Point(96, 29);
            this.picRainbow.Name = "picRainbow";
            this.picRainbow.Size = new System.Drawing.Size(176, 134);
            this.picRainbow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picRainbow.TabIndex = 1;
            this.picRainbow.TabStop = false;
            // 
            // lblResult
            // 
            this.lblResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResult.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.Location = new System.Drawing.Point(117, 301);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(131, 30);
            this.lblResult.TabIndex = 10;
            this.lblResult.Tag = "";
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnCalc
            // 
            this.btnCalc.BackColor = System.Drawing.Color.DarkGray;
            this.btnCalc.Location = new System.Drawing.Point(145, 169);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(75, 23);
            this.btnCalc.TabIndex = 13;
            this.btnCalc.Text = "Calculate";
            this.btnCalc.UseVisualStyleBackColor = false;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(362, 346);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.picResistor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbo3);
            this.Controls.Add(this.lbo2);
            this.Controls.Add(this.lbo1);
            this.Controls.Add(this.picRainbow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resistor Colour Calculator";
            this.TransparencyKey = System.Drawing.Color.Magenta;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picResistor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRainbow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbo1;
        private System.Windows.Forms.PictureBox picRainbow;
        private System.Windows.Forms.ListBox lbo2;
        private System.Windows.Forms.ListBox lbo3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picResistor;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Button btnCalc;
    }
}

